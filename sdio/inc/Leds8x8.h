/*
 * Leds8x8.h
 *
 *  Created on: 17 de may. de 2017
 *      Author: Bernardo
 */

#ifndef LEDS8X8_H_
#define LEDS8X8_H_
#include "main.h"
void LED8x8_init (void);
void LED8x8_imagen(int n);
void LED8x8_animacion(int n);
#endif /* LEDS8X8_H_ */
