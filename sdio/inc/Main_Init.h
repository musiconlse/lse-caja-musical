#ifndef __MAIN_INIT_H
#define __MAIN_INIT_H
#include "main.h"

void MX_GPIO_Init(void);
void MX_SDIO_SD_Init(void);
void MX_USART2_UART_Init(void);
void MX_SPI1_Init(void);
void HAL_ADC_Set(void);
void I2C_Set(void);
//void LED8x8_init(void);

#endif /* __MAIN_H */
