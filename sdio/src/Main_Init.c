/*
 * Main_Init.c
 *
 *  Created on: 17 de may. de 2017
 *      Author: Bernardo
 */

#include "Main_Init.h"

extern ADC_HandleTypeDef    AdcHandle;
extern UART_HandleTypeDef huart2;
extern SPI_HandleTypeDef hspi1;
extern SD_HandleTypeDef hsd;
extern uint8_t Dato_uart;
extern I2C_HandleTypeDef I2cHandle;

 void MX_SPI1_Init(void)
{
  //hspi1.Instance = SPI1;
  hspi1.Instance = SPI2;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_128;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
}
 void MX_SDIO_SD_Init(void)
{
  hsd.Instance = SDIO;
  hsd.Init.ClockEdge = SDIO_CLOCK_EDGE_RISING;
  hsd.Init.ClockBypass = SDIO_CLOCK_BYPASS_DISABLE;
  hsd.Init.ClockPowerSave = SDIO_CLOCK_POWER_SAVE_DISABLE;
  hsd.Init.BusWide = SDIO_BUS_WIDE_1B;
  hsd.Init.HardwareFlowControl = SDIO_HARDWARE_FLOW_CONTROL_DISABLE;
  hsd.Init.ClockDiv = 4;
}
 void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  /*Configure GPIO pin : PA0 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
  HAL_NVIC_SetPriority(EXTI4_IRQn, 0x0F, 0x00);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);
  /*Configure GPIO pin : PA0 CONTROLA INTERR. PARA 5V npn */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PA1 CONTROLA INTERR. PARA 5V npn */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}

void MX_USART2_UART_Init(void)
{
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  if(HAL_UART_Receive_IT(&huart2, &Dato_uart,1) != HAL_OK)
  {
    Error_Handler();
  }
}

HAL_ADC_Set(void)
{
	ADC_ChannelConfTypeDef sConfig;
	AdcHandle.Instance = ADCx;
	AdcHandle.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2;
	AdcHandle.Init.Resolution = ADC_RESOLUTION_12B;
	AdcHandle.Init.ScanConvMode = DISABLE;
	AdcHandle.Init.ContinuousConvMode = ENABLE;
	AdcHandle.Init.DiscontinuousConvMode = DISABLE;
	AdcHandle.Init.NbrOfDiscConversion = 0;
	AdcHandle.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	AdcHandle.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T1_CC1;
	AdcHandle.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	AdcHandle.Init.NbrOfConversion = 1;
	AdcHandle.Init.DMAContinuousRequests = ENABLE;
	AdcHandle.Init.EOCSelection = DISABLE;
	HAL_ADC_Init(&AdcHandle);
	sConfig.Channel = ADCx_CHANNEL;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
	sConfig.Offset = 0;
	HAL_ADC_ConfigChannel(&AdcHandle, &sConfig);
}
void I2C_Set(void){
	    I2cHandle.Instance             = I2C3;
	    I2cHandle.Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
	    I2cHandle.Init.ClockSpeed      = 100000;
	    I2cHandle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	    I2cHandle.Init.DutyCycle       = I2C_DUTYCYCLE_16_9;
	    I2cHandle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	    I2cHandle.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLE;
	    I2cHandle.Init.OwnAddress1     = I2C_ADDRESS;
	    I2cHandle.Init.OwnAddress2     = 0;
	    HAL_I2C_MspInit2(&I2cHandle);
	    if(HAL_I2C_Init(&I2cHandle) != HAL_OK)
	    {
	  	  Error_Handler();
	    }
}


