/**
  ******************************************************************************
  * @file    Audio_playback_and_record/Src/waveplayer.c 
  * @author  MCD Application Team
  * @version V1.0.4
  * @date    17-February-2017
  * @brief   I2S Audio player program. 
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright � 2017 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#define AUDIO_BUFFER_SIZE             4096
__IO uint32_t LEDsState;
__IO int16_t X_Offset, Y_Offset;
// Audio Play Start variable.
__IO uint32_t AudioPlayStart = 0;
/* Position in the audio play buffer */
__IO BUFFER_StateTypeDef BufferOffset = BUFFER_OFFSET_NONE;
extern __IO uint32_t RepeatState, PauseResumeStatus, PressCount;
extern uint8_t Dato_uart;
extern int count1;
/* Variable used to indicate audio mode (play, record or stop). */
extern __IO uint32_t CmdIndex;
/* Variable to indicate USB state (start/idle) */
extern MSC_ApplicationTypeDef AppliState;
static int16_t ThreadholdAcceleroLow = -10000, ThreadholdAcceleroHigh = 10000;
static int16_t MiddleLow = -1000, MiddleHigh = 1000;
/* Audio wave data length to be played */
static uint32_t WaveDataLength = 0;
/* Audio wave remaining data length to be played */
static __IO uint32_t AudioRemSize = 0;
/* Initial Volume level (from 0 (Mute) to 100 (Max)) */
static uint8_t Volume = 72;
int16_t Buffer[3];
uint8_t state = 1;
uint8_t state2 = 1;
uint8_t mute_case = 1;
/* Ping-Pong buffer used for audio play */
uint8_t Audio_Buffer[AUDIO_BUFFER_SIZE];
int vol=0;
int activa=0;
int rn=0;
/* Variable used by FatFs*/
FIL FileRead;
DIR Directory;

/**
  * @brief  Plays Wave from a mass storage.
  * @param  AudioFreq: Audio Sampling Frequency
  * @retval None
*/
void WavePlayBack(uint32_t AudioFreq)
{
	rn=0;
	rn=(rand()%5);
	UINT bytesread = 0;
  
	/* Start playing */
	AudioPlayStart = 1;
	RepeatState = REPEAT_ON;
  
  /* Initialize Wave player (Codec, DMA, I2C) */
	if(WavePlayerInit(AudioFreq) != 0)
	{	BSP_LED_On(LED3);
    	Error_Handler();
	}
  /* Get Data from USB Flash Disk */
	f_lseek(&FileRead, 0);
	f_read (&FileRead, &Audio_Buffer[0], AUDIO_BUFFER_SIZE, &bytesread);
	AudioRemSize = WaveDataLength - bytesread;
  /* Start playing Wave */
	BSP_AUDIO_OUT_Play((uint16_t*)&Audio_Buffer[0], AUDIO_BUFFER_SIZE);
	PauseResumeStatus = RESUME_STATUS;
  //PressCount = 0;
	rn=(rand()%5);
	LED8x8_animacion(rn);
  /* Check if the device is connected.*/
  while((AudioRemSize != 0))// && (AppliState != APPLICATION_IDLE))
  {
    if(CmdIndex == CMD_PLAY){
    	activa=1;
     	if(count1==160 || count1==320 || count1==499  ){
     		LED8x8_animacion(rn);}
   	  	if(count1==660|| count1==820 ||  count1==999 ){
   	  		vol=0;
   	  		LED8x8_animacion(rn+5);
   	  	}
    	Buffer[0] = 0;
    	/* MEMS Accelerometer configure */
    	BSP_ACCELERO_GetXYZ(Buffer);
    	/* Set X position */
    	////VOLUMEN
    	X_Offset = Buffer[1];
    	if ((X_Offset < ThreadholdAcceleroLow||Dato_uart=='b') && state == 0)
    	  	{if(Dato_uart=='b'){
    	  		vol=1;}
    	  	LED8x8_imagen(4);
    	  	Volume = Volume - 4;
    	  	if (Volume < 40)
    	  	{
    	  		Volume = 40;
    	  	 }
    	  	BSP_AUDIO_OUT_SetVolume(Volume);
    	  	state = 1;
    	  	Dato_uart='1';
    	  	}

    	else if ((X_Offset > ThreadholdAcceleroHigh ||Dato_uart=='a')&& state == 0)
    	  	{
    	  	  if(Dato_uart=='a'){
    	  		  vol=1;}
    	  	  LED8x8_imagen(5);
    	  	  Volume = Volume + 4;
    	  	  if (Volume > 92)
    	  	  {
    	  		  Volume = 92;
    	  	  }
    	  	  BSP_AUDIO_OUT_SetVolume(Volume);
    	  	  state = 1;
    	  	  Dato_uart='1';
    	  	}

    	 else if (MiddleLow < X_Offset && X_Offset < MiddleHigh && state == 1)
    	  	{
    	  	 if(vol!=1){
    	  		 LED8x8_imagen(0);
    	  	 }
    	  	 state = 0;
    	     }
    	////FUNCIONES APLICACION MOVIL
    	 if (Dato_uart=='c' && state2 == 0)
    	  		{
    	  		  BSP_LED_Off(LED6);
    	  		  state2 = 1;
        	  	  Dato_uart='1';
        	  	  nivel_Bateria(2);
    	  		}

    	  else if (Dato_uart=='m' && state2 == 0)
    	  		{
    	  		  switch(mute_case)
    	  		  {
    	  		  case 1:
    	  		    BSP_AUDIO_OUT_SetMute(AUDIO_MUTE_ON);
    	  		    mute_case = 2;
    	  		    break;
    	  		  case 2:
    	  		    BSP_AUDIO_OUT_SetMute(AUDIO_MUTE_OFF);
    	  		    mute_case = 1;
    	  		    break;
    	  		  }
    	  		  state2 = 1;
      	  	      Dato_uart='1';
    	  		}
    	  else if (Dato_uart=='r' && state2 == 0)
    	  		  {	f_unlink("Lista.txt" );
    	  		    state2 = 1;
    	  		    Dato_uart='1';
    	  		  }

    	  else if (Dato_uart=='s'&& state2 == 0)
    	  		{ activa=0;
    	  	      /* Stop Player before close Test */
    	  		  if (BSP_AUDIO_OUT_Stop(CODEC_PDWN_HW) != AUDIO_OK)
    	  		  {
    	  		    /* Audio Stop error */
    	  			Error_Handler();
    	  		  }
    	  		  f_close(&FileRead);
    	  		  state2 = 1;
    	  		  Dato_uart='1';
    	  		  break;
    	  		}
    	   else if (Dato_uart=='1' && state2 == 1)
    	  		{
    	  		  BSP_LED_On(LED6);
    	  		  state2 = 0;
    	  		}
   ///////////////////////////////////////////////////////////////////////////////////////////
      if(PauseResumeStatus == PAUSE_STATUS)
      {
        /* Pause playing Wave */
        WavePlayerPauseResume(PauseResumeStatus);
        PauseResumeStatus = IDLE_STATUS;
      }
      else if(PauseResumeStatus == RESUME_STATUS)
      {
        /* Resume playing Wave */
        WavePlayerPauseResume(PauseResumeStatus);
        PauseResumeStatus = IDLE_STATUS;
      }  
      
      bytesread = 0;
      
      if(BufferOffset == BUFFER_OFFSET_HALF)
      {
        f_read(&FileRead, 
               &Audio_Buffer[0], 
               AUDIO_BUFFER_SIZE/2, 
               (void *)&bytesread); 
        
        BufferOffset = BUFFER_OFFSET_NONE;
      }
      if(BufferOffset == BUFFER_OFFSET_FULL)
      {
        f_read(&FileRead, 
               &Audio_Buffer[AUDIO_BUFFER_SIZE/2], 
               AUDIO_BUFFER_SIZE/2, 
               (void *)&bytesread);
        BufferOffset = BUFFER_OFFSET_NONE;
      } 
      if(AudioRemSize > (AUDIO_BUFFER_SIZE / 2))
      {
        AudioRemSize -= bytesread;
      }
      else
      {
        AudioRemSize = 0;
      }
    }
    else 
    {
      /* Stop playing Wave */
      WavePlayerStop();
      f_close(&FileRead);
      AudioRemSize = 0;
      RepeatState = REPEAT_ON;
      break;
    }
  }
#ifdef PLAY_REPEAT_DISABLED 
  RepeatState = REPEAT_OFF;
  /* Stop playing Wave */
  WavePlayerStop();
  f_close(&FileRead);
  /* Test on the command: Playing */
  if(CmdIndex == CMD_PLAY)
  {
    LEDsState = LED4_TOGGLE;
  }
#else 
//  LEDsState = LEDS_OFF;
  RepeatState = REPEAT_ON;
  AudioPlayStart = 0;
  /* Stop playing Wave */
  WavePlayerStop();
  f_close(&FileRead);
#endif /* PLAY_REPEAT_DISABLED */
}

/**
  * @brief  Pauses or Resumes a played Wave.
  * @param  state: Player state: Pause, Resume or Idle
  * @retval None
  */
void WavePlayerPauseResume(uint32_t wState)
{ 
  if(wState == PAUSE_STATUS)
  {
    BSP_AUDIO_OUT_Pause();   
  }
  else
  {
    BSP_AUDIO_OUT_Resume();   
  }
}

/**
  * @brief  Stops playing Wave.
  * @param  None
  * @retval None
  */
void WavePlayerStop(void)
{ 
  BSP_AUDIO_OUT_Stop(CODEC_PDWN_HW);
}
 
/**
  * @brief  Initializes the Wave player.
  * @param  AudioFreq: Audio sampling frequency
  * @retval None
  */
int WavePlayerInit(uint32_t AudioFreq)
{ 
  /* Initialize the Audio codec and all related peripherals (I2S, I2C, IOExpander, IOs...) */  
  return(BSP_AUDIO_OUT_Init(OUTPUT_DEVICE_AUTO, Volume, AudioFreq));  
}

/*--------------------------------
Callbacks implementation:
The callbacks prototypes are defined in the stm32f4_discovery_audio_codec.h file
and their implementation should be done in the user code if they are needed.
Below some examples of callback implementations.
--------------------------------------------------------*/

/**
  * @brief  Manages the DMA Half Transfer complete interrupt.
  * @param  None
  * @retval None
  */
void BSP_AUDIO_OUT_HalfTransfer_CallBack(void)
{ 
  BufferOffset = BUFFER_OFFSET_HALF;
}

/**
* @brief  Calculates the remaining file size and new position of the pointer.
* @param  None
* @retval None
*/
void BSP_AUDIO_OUT_TransferComplete_CallBack(void)
{
  BufferOffset = BUFFER_OFFSET_FULL;
  BSP_AUDIO_OUT_ChangeBuffer((uint16_t*)&Audio_Buffer[0], AUDIO_BUFFER_SIZE /2);
}

/**
* @brief  Manages the DMA FIFO error interrupt.
* @param  None
* @retval None
*/
void BSP_AUDIO_OUT_Error_CallBack(void)
{
  /* Stop the program with an infinite loop */
  while (1)
  {}
  /* Could also generate a system reset to recover from the error */
  /* .... */
}

/**
  * @brief  Starts Wave player.
  * @param  None
  * @retval None
  */
void WavePlayerStart(char* name)
{
  UINT bytesread = 0;
  char path[] = "0:/";
  char* wavefilename = NULL;
  WAVE_FormatTypeDef waveformat;
  
  /* Get the read out protection status */
  if(f_opendir(&Directory, path) == FR_OK)
  {BSP_LED_On(LED6);
      wavefilename = name;
    /* Open the Wave file to be played */
    if(f_open(&FileRead, wavefilename , FA_READ) != FR_OK)
    {
      BSP_LED_On(LED5);
    }
    else
    {    
      /* Read sizeof(WaveFormat) from the selected file */
      f_read (&FileRead, &waveformat, sizeof(waveformat), &bytesread);
      /* Set WaveDataLenght to the Speech Wave length */
      WaveDataLength = waveformat.FileSize;
      /* Play the Wave */
      WavePlayBack(waveformat.SampleRate);
    }    
  }
}

/**
  * @brief  Resets the Wave player.
  * @param  None
  * @retval None
  */
void WavePlayer_CallBack(void)
{
  if(AppliState != APPLICATION_IDLE)
  {
    /* Reset the Wave player variables */
    RepeatState = REPEAT_ON;
    AudioPlayStart = 0;
    PauseResumeStatus = RESUME_STATUS;
    WaveDataLength =0;
    PressCount = 0;
    
    /* Stop the Codec */
    if(BSP_AUDIO_OUT_Stop(CODEC_PDWN_HW) != AUDIO_OK)
    {
      while(1){};
    }
    /* Turn OFF LED3, LED4 and LED6 */
    BSP_LED_Off(LED3);
    BSP_LED_Off(LED4);
    BSP_LED_Off(LED6);
  }
} 

/**
* @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
