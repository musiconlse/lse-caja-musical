/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
__IO uint32_t RepeatState = REPEAT_ON;
__IO uint16_t CCR1Val = 16826;
__IO uint32_t CmdIndex = CMD_STOP;
__IO uint32_t PbPressCheck = 0;
__IO uint32_t PauseResumeStatus = IDLE_STATUS;
__IO uint32_t PressCount = 0;
__IO int direc=0;
__IO uint16_t ADCValue = 0;
__IO ITStatus UartReady = RESET;
ADC_HandleTypeDef    AdcHandle;
UART_HandleTypeDef huart2;
SPI_HandleTypeDef hspi1;
SD_HandleTypeDef hsd;
I2C_HandleTypeDef I2cHandle;
FATFS SDFatFs;  /* File system object for SD disk logical drive */
FIL MyFile;     /* File object */
FILINFO fno;
DIR Directory;
char SDPath[4];
char path[] = "0:/";
char *fn;
extern int activa;
int flag=0;
int bat=0;
int count1=0;
static int antirebote=0,estado=0;
static int count=0,n_song=0,rfidv=0;
static int count2=0,count3=0,rfidn=0;
uint8_t CardID[5];
uint8_t Dato_uart='1';
uint8_t adc1[2];
uint8_t dataRx[32];
uint32_t byteswritten;
unsigned char Cmd_B[2];
static unsigned char fn_tmp[25][25],name[20];
static unsigned char arrayA[25],arrayB[25];
static unsigned char Cmd_A[20],Cmd_C[20],Cmd_D[DATA_BUFFER];
static void SystemClock_Config(void);
static void MSC_Application(void);
static void lectura_RFID(void);
static void lectura_memoria(void);
void Error_Handler(void);
void nuevo_tag(void);
void nivel_Bateria(int v);

int main(void)
{
  HAL_Init();
  SystemClock_Config();
  MX_GPIO_Init();
  MX_SDIO_SD_Init();
  MX_FATFS_Init();
  MX_USART2_UART_Init();
  MX_SPI1_Init();
  //TM_MFRC522_Init();
  HAL_ADC_Set();
  HAL_ADC_Start(&AdcHandle);
  BSP_ACCELERO_Init();
  BSP_LED_Init(LED3);
  BSP_LED_Init(LED4);
  BSP_LED_Init(LED5);
  BSP_LED_Init(LED6);
  BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_EXTI);
  MSC_Application();
  I2C_Set();
  LED8x8_init();
  LED8x8_imagen(0);
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_SET);
  srand(2);
	 while (1)
    {
		 switch(estado){
		 case 0: if(count2==10){
				 	 nivel_Bateria(1);
				 	 //HAL_Delay(750);
				 }
		 	 	 if(count2>=4000){
		 	 		 LED8x8_imagen(0);
		 	 	 }count3=0;
		         flag=0;
				 BSP_LED_Off(LED3);
				 BSP_LED_Off(LED4);
				 BSP_LED_Off(LED5);
				 BSP_LED_Off(LED6);
		         break;
		 case 1: if(rfidv==0&&rfidn==0){
			 	 	 lectura_memoria();
			 	 	 count3=0;
		 	 	 }
		         else if( Dato_uart=='n' && rfidn==1){
		        	 LED8x8_imagen(6);
		        	 HAL_UART_Transmit(&huart2, arrayB, strlen(arrayB),500);
			     	 nuevo_tag();}
			     else{
			    	 if(count3>=15000||rfidn==0){
			    		 estado=0;
			    		 count2=0;
			    		 rfidv=0;
			    		 rfidn=0;
			    		 CardID[0]=0;
			    		 CardID[1]=0;
			    		 activa=0;
			    		 HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_RESET);
			    		 HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_SET);
			     }}
		 		 break;
		 default: break;
		 }
}}

//Interrupción fin de Carrera
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GPIO_PIN_4){
	  if (antirebote==0){
	      antirebote=1;
	      count=0;
	      if(rfidn==1){
	    	  rfidn=0;
	    	  rfidv=0;
	      }
	      if (activa==1){
	    	  Dato_uart='s';
	    	  CmdIndex =CMD_STOP;}
	      else{
	    	  LED8x8_init();
	    	  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_RESET);
	    	  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_SET);
	    	  HAL_Delay(100);
	    	  estado=1;
	    	  lectura_RFID();
	    	  CmdIndex =CMD_PLAY;}
  }}}

//Inicialización de la memoria
static void MSC_Application(void)
 {
	 if(f_mount(&SDFatFs, (TCHAR const*)SDPath, 0) != FR_OK)
   {
     Error_Handler();
   }
}
//Consulta del nivel de bateria
void nivel_Bateria(int v)
 {
     ADCValue = HAL_ADC_GetValue(&AdcHandle);
     adc1[0]=(uint8_t)(ADCValue>>8);
     adc1[1]=(uint8_t)ADCValue;
     if(ADCValue>=2650){
    	  bat=100;
    	  LED8x8_imagen(11);}
      else if(ADCValue>=1720){
    	  bat=75;
    	  LED8x8_imagen(10);}
      else if (ADCValue>=1200){
    	  bat=50;
    	  LED8x8_imagen(9);}
      else if(ADCValue>=200){
    	  bat=25;
    	  LED8x8_imagen(8);}
      else{;
          bat=5;
      	  LED8x8_imagen(7);}
     if(v==2){
     sprintf(arrayA,"%d",bat);
     HAL_UART_Transmit_IT(&huart2,(uint8_t *)arrayA,strlen(arrayA));
     v=1;}
 }
//Lectura tarjeta RFID
static void lectura_RFID(){
	 CardID[0]=0;
	 CardID[1]=0;
	 TM_MFRC522_Init();
	 HAL_Delay(50);
	 MSC_Application();
	 TM_MFRC522_Check(CardID);
	 sprintf(arrayB, "tag: %x, %x, %x, %x, %x\n", CardID[0] ,CardID[1] , CardID[2] ,CardID[3] ,CardID[4]);
	// HAL_UART_Transmit_IT(&huart2, arrayB, strlen(arrayB));
	 HAL_Delay(100);
}
//Lectura de memoria canciones guardadas
static void lectura_memoria(void){
	int j=0;
	if(f_opendir(&Directory, path) == FR_OK)
		  {	sprintf(Cmd_C, "%x%x", CardID[0] ,CardID[1]);
			if(f_open(&MyFile, "Lista.txt" , FA_OPEN_ALWAYS|FA_READ) != FR_OK){
				Error_Handler();}
			f_lseek(&MyFile,0);
			if(f_read(&MyFile, Cmd_D, MyFile.fsize, &byteswritten)== FR_OK){
				for(int i=0;i<MyFile.fsize;i++){
					if(Cmd_D[i]==Cmd_C[rfidv]&& rfidv<strlen(Cmd_C)){
						rfidv++;}
					else{
						if(rfidv>=strlen(Cmd_C)){
							if(Cmd_D[i]=='+'){
								f_close(&MyFile);
								WavePlayerStart(name);
								activa=0;
								break;}
							else{
								name[j]=Cmd_D[i];
								j++;}}
						else{
							rfidv=0;}}}
				if(j==0){
					LED8x8_imagen(1);
					rfidn=1;}
			    f_close(&MyFile);
		    	f_closedir (&Directory);
   	  }}}
// Guardar una nueva Tarejeta en memoria
void nuevo_tag(void){
	n_song=1;
	flag=1;
	if(f_opendir(&Directory, path) == FR_OK){
		for (;;) {
			if( f_readdir (&Directory, &fno)== FR_OK && fno.fname[0] != 0){
				if (fno.fattrib !=AM_SYS) {
					fn = fno.fname;
					sprintf(fn, "%s\n",fn);
					for(int i=0;i<strlen(fn)-4;i++){
						if(fn[i]=='.'&&fn[i+1]=='W'&&fn[i+2]=='A'&&fn[i+3]=='V'){
							HAL_UART_Transmit(&huart2,fn,strlen(fn),1000);
							strcpy(&fn_tmp[n_song],fn);
							n_song++;
	    		}}}
			}else{
				while (1){
					if(Dato_uart-48>0 && Dato_uart-48<n_song){
						sprintf(Cmd_A, "+%x%x%s+", CardID[0] ,CardID[1],&fn_tmp[(uint8_t)Dato_uart-48]);
						if(f_open(&MyFile, "Lista.txt" , FA_OPEN_ALWAYS| FA_WRITE|FA_READ) == FR_OK){
							f_lseek(&MyFile, MyFile.fsize);
							if(f_write(&MyFile, Cmd_A, strlen(Cmd_A), &byteswritten)== FR_OK){}
							LED8x8_imagen(2);
							HAL_Delay(1000);
							f_close(&MyFile);
							flag=0;
							break;}
	        		}}
	    	    f_closedir (&Directory);
	    	    break;}}}
	else{
		Error_Handler();
	}}
//Interrupción timer del sistema
void HAL_SYSTICK_Callback(void)
{
	if (count >= 750) {
		count = 0;
		antirebote=0;
	}
	else {count++;}
	if (count1 > 1000) {
			 count1=0;
	}
	else {count1++;}
	if (count2 >= 4000) {
		 count2=4000;
		}
	else {count2++;}
	if (count3 >= 15000) {
		 count3=15000;
		}
	else {count3++;}
}

//Interrupción recepción uart
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(HAL_UART_Receive_IT(huart,&Dato_uart, 1) != HAL_OK){
		Error_Handler();}
}

void HAL_SPI_RxCpltCallback (SPI_HandleTypeDef *hspi)
{
	if (hspi->Instance==hspi1.Instance){
		HAL_SPI_Receive_IT (&hspi1, (uint8_t *)dataRx, 10);}
}
void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *I2cHandle)
{
 	Error_Handler();
}
//Configuración reloj del sistema 100Mhz
static void SystemClock_Config(void)
{
 RCC_ClkInitTypeDef RCC_ClkInitStruct;
 RCC_OscInitTypeDef RCC_OscInitStruct;
 /* Enable Power Control clock */
 __HAL_RCC_PWR_CLK_ENABLE();
 __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  /* Enable HSI Oscillator and activate PLL with HSI as src */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 0x10;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 400;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
	  Error_Handler();
  }
  RCC_ClkInitStruct.ClockType =
  (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK |
  RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) !=
 HAL_OK)
  {
	  Error_Handler();
  }
    /**Configure the Systick interrupt time   */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
    /**Configure the Systick  */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 1, 0x01);
}

void Error_Handler(void)
{
	BSP_LED_Toggle(LED5);
}

#ifdef USE_FULL_ASSERT
/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

